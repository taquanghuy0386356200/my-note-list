import 'dart:async';
import 'dart:math';

import 'package:todo_list/base/base_bloc.dart';
import 'package:todo_list/base/base_event.dart';
import 'package:todo_list/event/add_todo_event.dart';
import 'package:todo_list/event/delete_toto_event.dart';
import 'package:todo_list/model/todo.dart';
class TodoBloc extends BaseBloc {
  StreamController <List<Todo>> _todoListStreamController = StreamController<List<Todo>>();
  Stream<List<Todo>> get todoListStream => _todoListStreamController.stream;
  var random = Random();

  List<Todo> todoList = [];

  _addTodo(Todo todo) {
    todoList.add(todo);
    _todoListStreamController.sink.add(todoList);
  }

  _deleteTodo(Todo todo) {
    todoList.remove(todo);
    _todoListStreamController.sink.add(todoList);
  }

  @override
  void dispatchEvent(BaseEvent event) {
    if(event is AddTodoEvent) {
      Todo todo = Todo.fromData(random.nextInt(10000), event.content);
      _addTodo(todo);
      print(event.content);
    } else if(event is DeleteTodoEvent) {
      _deleteTodo(event.todo);
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _todoListStreamController.close();
  }
}