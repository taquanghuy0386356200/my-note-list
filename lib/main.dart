import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/bloc/todo_bloc.dart';
import 'package:todo_list/model/todo.dart';
import 'package:todo_list/todo/todo_list_container.dart';

// package base để tạo những khởi tạo cơ sở cho event và bloc

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: Scaffold(
          appBar: AppBar(
            title: Text('Todo List'),
          ),
          //sau khi add provider thi cac widget trong cai todolistcontainer duoc asset vao bloc va su dung cac
          //phuong thuc trong bloc
          body: Provider<TodoBloc>.value(
              value: TodoBloc(),
              child: ToDoListContainer()),
        ));
  }
}
