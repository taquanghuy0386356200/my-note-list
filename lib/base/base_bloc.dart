//chứa stream controller để nhận events, quản lí từ widget đẩy event đi
//dispatch: gui di ~ send
import 'dart:async';
import 'package:todo_list/base/base_event.dart';

abstract class BaseBloc {
  // ignore: close_sinks
  StreamController<BaseEvent> _eventStreamController = StreamController<BaseEvent>();

  Sink<BaseEvent> get event => _eventStreamController.sink;

  BaseBloc() {
    _eventStreamController.stream.listen((event) {
      if(event is! BaseEvent) {
        throw Exception('Event không hợp lệ');
      }

      dispatchEvent(event);
    });
  }

  void dispatchEvent(BaseEvent event);

  void dispose() {
    _eventStreamController.close();
  }
}