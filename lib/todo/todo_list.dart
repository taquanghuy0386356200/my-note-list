import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_list/bloc/todo_bloc.dart';
import 'package:todo_list/event/delete_toto_event.dart';
import 'package:todo_list/model/todo.dart';

class TodoList extends StatefulWidget {
  const TodoList({Key? key}) : super(key: key);

  @override
  _TodoListState createState() => _TodoListState();
}

class _TodoListState extends State<TodoList> {
  Icon? ok;

  @override
  Widget build(BuildContext context) {
    var bloc = Provider.of<TodoBloc>(context);
    return Consumer<TodoBloc>(
      builder: (context, bloc, child) => StreamBuilder<List<Todo>>(
        stream: bloc.todoListStream,
        builder: (context, snapshot) {

          switch(snapshot.connectionState) {
            case ConnectionState.active:
              return ListView.builder(
                  itemCount: snapshot.data!.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(snapshot.data![index].content),
                      trailing: GestureDetector(
                          onTap: () {
                            bloc.event.add(DeleteTodoEvent(snapshot.data![index]));
                          },
                          child: Icon(
                            Icons.delete,
                            color: Colors.red[400],
                          )),
                    );
                  });
            case ConnectionState.none:
            default:
              return Center(
                child: Container(
                  height: 70,
                  width: 70,
                  child: CircularProgressIndicator(),
                ),
              );
          }
        },
      ),
    );
  }
}
