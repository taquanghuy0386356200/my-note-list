import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/todo/todo_header.dart';
import 'package:todo_list/todo/todo_list.dart';

class ToDoListContainer extends StatelessWidget {
  // const MyHomePage({Key? key, title: this.title}) : super(key: key, title);
  // String title;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          TodoListHeader(),
          Expanded(
            child: TodoList(),
          )
        ],
      ),
    );
  }
}
